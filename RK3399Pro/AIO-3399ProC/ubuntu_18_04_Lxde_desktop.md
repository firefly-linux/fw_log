
## Date: 2022-08-24
* Firmware name: AIO-RK3399PROC_Ubuntu18.04-r21156_v1.4.2a_220824.img
* Firmware MD5: d7ab4c836ae31ab3ce95e5d6f68933cd
* rootfs: Ubuntu18.04-Lxde_RK3399PRO_v2.11-56_20220527.img

Update content:

* rk3399pro-firefly-aioc: NPU: Optimize the transmission speed of RK1808 using PCIE bus

* Rootfs:
    1. Add built-in support and install rknn-toolkit version 1.7.1 (mainly for RK3399Pro)
    2. Support ec200 dialing using AT command as default
    3. Firmware: update nvram_ap6275s.txt
    4. Add AP6398SV wifi and bt firmware
    5. Move /system/etc/firmware/[wifi_firmware] to /vendor/etc/firmware
    6. EC20: quectel-CM: update to v1.6.1
    7. Firmware: upgrade ap6256 firmware
    8. Rt5640: Repair recording function


## Date: 2022-08-24
* Firmware name: AIO-RK3399PROC-LVDS_Ubuntu18.04-r21156_v1.4.2a_220824.img
* Firmware MD5: faedecabf9bac2e867155fe34a31d357
* rootfs: Ubuntu18.04-Lxde_RK3399PRO_v2.11-56_20220527.img

Update content:

* rk3399pro-firefly-aioc-lvds.dts: Adapt to gsl touch screen
* rk3399pro-firefly-aioc: NPU: Optimize the transmission speed of RK1808 using PCIE bus

* Rootfs:
    1. Add built-in support and install rknn-toolkit version 1.7.1 (mainly for RK3399Pro)
    2. Support ec200 dialing using AT command as default
    3. Firmware: update nvram_ap6275s.txt
    4. Add AP6398SV wifi and bt firmware
    5. Move /system/etc/firmware/[wifi_firmware] to /vendor/etc/firmware
    6. EC20: quectel-CM: update to v1.6.1
    7. Firmware: upgrade ap6256 firmware
    8. Rt5640: Repair recording function




## Date: 2022-08-24
* Firmware name: AIO-RK3399PROC_Ubuntu18.04-Minimal-r21156_v1.4.2a_220824.img
* Firmware MD5: a94bb2ec87e9eca35884ba46774b905b
* rootfs: Ubuntu18.04-Minimal_RK3399PRO_v2.11-56_20220526.img

Update content:

* rk3399pro-firefly-aioc: NPU: Optimize the transmission speed of RK1808 using PCIE bus

* Rootfs:
    1. Add built-in support and install rknn-toolkit version 1.7.1 (mainly for RK3399Pro)
    2. Support ec200 dialing using AT command as default
    3. Firmware: update nvram_ap6275s.txt
    4. Add AP6398SV wifi and bt firmware
    5. Move /system/etc/firmware/[wifi_firmware] to /vendor/etc/firmware
    6. EC20: quectel-CM: update to v1.6.1
    7. Firmware: upgrade ap6256 firmware
    8. Rt5640: Repair recording function


## Date: 2022-08-24
* Firmware name: AIO-RK3399PROC-LVDS_Ubuntu18.04-Minimal-r21156_v1.4.2a_220824.img
* Firmware MD5: 4e042a7b2711c83314e80d91ca81e6ba
* rootfs: Ubuntu18.04-Minimal_RK3399PRO_v2.11-56_20220526.img

Update content:

* rk3399pro-firefly-aioc-lvds.dts: Adapt to gsl touch screen
* rk3399pro-firefly-aioc: NPU: Optimize the transmission speed of RK1808 using PCIE bus

* Rootfs:
    1. Add built-in support and install rknn-toolkit version 1.7.1 (mainly for RK3399Pro)
    2. Support ec200 dialing using AT command as default
    3. Firmware: update nvram_ap6275s.txt
    4. Add AP6398SV wifi and bt firmware
    5. Move /system/etc/firmware/[wifi_firmware] to /vendor/etc/firmware
    6. EC20: quectel-CM: update to v1.6.1
    7. Firmware: upgrade ap6256 firmware
    8. Rt5640: Repair recording function



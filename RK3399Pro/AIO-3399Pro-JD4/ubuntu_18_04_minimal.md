
## Date: 2022-08-24
* Firmware name: AIO-RK3399PRO-JD4_Ubuntu18.04-Minimal-r21156_v1.4.2a_220824.img
* Firmware MD5: 8d473542dd69c21a031ca48b07ae7d84
* rootfs: Ubuntu18.04-Minimal_RK3399PRO_v2.11-56_20220526.img

Update content:

* Upgrade kernel driver of rga2.
* wifi: update bcmdhd version 100.10.545.19 and support AP6398SV wifi
* Kernel Merge remote-tracking branch 'rk/stable-4.4-rk3399pro-linux'

* Rootfs:
    1. Add built-in support and install rknn-toolkit version 1.7.1 (mainly for RK3399Pro)
    2. Support ec200 dialing using AT command as default
    3. Firmware: update nvram_ap6275s.txt
    4. Add AP6398SV wifi and bt firmware
    5. Move /system/etc/firmware/[wifi_firmware] to /vendor/etc/firmware
    6. EC20: quectel-CM: update to v1.6.1
    7. Firmware: upgrade ap6256 firmware
    8. Rt5640: Repair recording function


## Date: 2022-08-24
* Firmware name: AIO-RK3399PRO-JD4-LVDS_Ubuntu18.04-Minimal-r21156_v1.4.2a_220824.img
* Firmware MD5: a24c59c525b9c7712f985bbe88ef3293
* rootfs: Ubuntu18.04-Minimal_RK3399PRO_v2.11-56_20220526.img

Update content:

* Upgrade kernel driver of rga2.
* wifi: update bcmdhd version 100.10.545.19 and support AP6398SV wifi
* Kernel Merge remote-tracking branch 'rk/stable-4.4-rk3399pro-linux'

* Rootfs:
    1. Add built-in support and install rknn-toolkit version 1.7.1 (mainly for RK3399Pro)
    2. Support ec200 dialing using AT command as default
    3. Firmware: update nvram_ap6275s.txt
    4. Add AP6398SV wifi and bt firmware
    5. Move /system/etc/firmware/[wifi_firmware] to /vendor/etc/firmware
    6. EC20: quectel-CM: update to v1.6.1
    7. Firmware: upgrade ap6256 firmware
    8. Rt5640: Repair recording function


